package com.yixinglab.bamai.web.rest;

import com.yixinglab.bamai.Application;
import com.yixinglab.bamai.domain.MoneyFlow;
import com.yixinglab.bamai.repository.MoneyFlowRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.exceptions.ExceptionIncludingMockitoWarnings;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the MoneyFlowResource REST controller.
 *
 * @see MoneyFlowResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class MoneyFlowResourceIntTest {

    private static final String DEFAULT_SOURCE = "AAAAA";
    private static final String UPDATED_SOURCE = "BBBBB";
    private static final String DEFAULT_DEST = "AAAAA";
    private static final String UPDATED_DEST = "BBBBB";

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(2);

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_SUB_GROUP = "AAAAA";
    private static final String UPDATED_SUB_GROUP = "BBBBB";
    private static final String DEFAULT_COMMENT = "AAAAA";
    private static final String UPDATED_COMMENT = "BBBBB";

    @Inject
    private MoneyFlowRepository moneyFlowRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restMoneyFlowMockMvc;

    private MoneyFlow moneyFlow;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MoneyFlowResource moneyFlowResource = new MoneyFlowResource();
        ReflectionTestUtils.setField(moneyFlowResource, "moneyFlowRepository", moneyFlowRepository);
        this.restMoneyFlowMockMvc = MockMvcBuilders.standaloneSetup(moneyFlowResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        moneyFlow = new MoneyFlow();
        moneyFlow.setSource(DEFAULT_SOURCE);
        moneyFlow.setDest(DEFAULT_DEST);
        moneyFlow.setAmount(DEFAULT_AMOUNT);
        moneyFlow.setDate(DEFAULT_DATE);
        moneyFlow.setSubGroup(DEFAULT_SUB_GROUP);
        moneyFlow.setComment(DEFAULT_COMMENT);
    }

    @Test
    @Transactional
    public void createMoneyFlow() throws Exception {
        int databaseSizeBeforeCreate = moneyFlowRepository.findAll().size();

        // Create the MoneyFlow

        restMoneyFlowMockMvc.perform(post("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isCreated());

        // Validate the MoneyFlow in the database
        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeCreate + 1);
        MoneyFlow testMoneyFlow = moneyFlows.get(moneyFlows.size() - 1);
        assertThat(testMoneyFlow.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testMoneyFlow.getDest()).isEqualTo(DEFAULT_DEST);
        assertThat(testMoneyFlow.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testMoneyFlow.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testMoneyFlow.getSubGroup()).isEqualTo(DEFAULT_SUB_GROUP);
        assertThat(testMoneyFlow.getComment()).isEqualTo(DEFAULT_COMMENT);
    }

    @Test
    @Transactional
    public void checkSourceIsRequired() throws Exception {
        int databaseSizeBeforeTest = moneyFlowRepository.findAll().size();
        // set the field null
        moneyFlow.setSource(null);

        // Create the MoneyFlow, which fails.

        restMoneyFlowMockMvc.perform(post("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isBadRequest());

        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDestIsRequired() throws Exception {
        int databaseSizeBeforeTest = moneyFlowRepository.findAll().size();
        // set the field null
        moneyFlow.setDest(null);

        // Create the MoneyFlow, which fails.

        restMoneyFlowMockMvc.perform(post("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isBadRequest());

        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = moneyFlowRepository.findAll().size();
        // set the field null
        moneyFlow.setAmount(null);

        // Create the MoneyFlow, which fails.

        restMoneyFlowMockMvc.perform(post("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isBadRequest());

        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = moneyFlowRepository.findAll().size();
        // set the field null
        moneyFlow.setDate(null);

        // Create the MoneyFlow, which fails.

        restMoneyFlowMockMvc.perform(post("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isBadRequest());

        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubGroupIsRequired() throws Exception {
        int databaseSizeBeforeTest = moneyFlowRepository.findAll().size();
        // set the field null
        moneyFlow.setSubGroup(null);

        // Create the MoneyFlow, which fails.

        restMoneyFlowMockMvc.perform(post("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isBadRequest());

        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMoneyFlows() throws Exception {
        // Initialize the database
        moneyFlowRepository.saveAndFlush(moneyFlow);

        // Get all the moneyFlows
        restMoneyFlowMockMvc.perform(get("/api/moneyFlows?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(moneyFlow.getId().intValue())))
                .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
                .andExpect(jsonPath("$.[*].dest").value(hasItem(DEFAULT_DEST.toString())))
                .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
                .andExpect(jsonPath("$.[*].subGroup").value(hasItem(DEFAULT_SUB_GROUP.toString())))
                .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void getMoneyFlow() throws Exception {
        // Initialize the database
        moneyFlowRepository.saveAndFlush(moneyFlow);

        // Get the moneyFlow
        restMoneyFlowMockMvc.perform(get("/api/moneyFlows/{id}", moneyFlow.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(moneyFlow.getId().intValue()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.dest").value(DEFAULT_DEST.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.subGroup").value(DEFAULT_SUB_GROUP.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMoneyFlow() throws Exception {
        // Get the moneyFlow
        restMoneyFlowMockMvc.perform(get("/api/moneyFlows/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMoneyFlow() throws Exception {
        // Initialize the database
        moneyFlowRepository.saveAndFlush(moneyFlow);

		int databaseSizeBeforeUpdate = moneyFlowRepository.findAll().size();

        // Update the moneyFlow
        moneyFlow.setSource(UPDATED_SOURCE);
        moneyFlow.setDest(UPDATED_DEST);
        moneyFlow.setAmount(UPDATED_AMOUNT);
        moneyFlow.setDate(UPDATED_DATE);
        moneyFlow.setSubGroup(UPDATED_SUB_GROUP);
        moneyFlow.setComment(UPDATED_COMMENT);

        restMoneyFlowMockMvc.perform(put("/api/moneyFlows")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moneyFlow)))
                .andExpect(status().isOk());

        // Validate the MoneyFlow in the database
        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeUpdate);
        MoneyFlow testMoneyFlow = moneyFlows.get(moneyFlows.size() - 1);
        assertThat(testMoneyFlow.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testMoneyFlow.getDest()).isEqualTo(UPDATED_DEST);
        assertThat(testMoneyFlow.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMoneyFlow.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testMoneyFlow.getSubGroup()).isEqualTo(UPDATED_SUB_GROUP);
        assertThat(testMoneyFlow.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void deleteMoneyFlow() throws Exception {
        // Initialize the database
        moneyFlowRepository.saveAndFlush(moneyFlow);

		int databaseSizeBeforeDelete = moneyFlowRepository.findAll().size();

        // Get the moneyFlow
        restMoneyFlowMockMvc.perform(delete("/api/moneyFlows/{id}", moneyFlow.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<MoneyFlow> moneyFlows = moneyFlowRepository.findAll();
        assertThat(moneyFlows).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void getGroupedFlowsByParticipant() throws Exception {
        moneyFlowRepository.saveAndFlush(createFlow("investor", "self", 10002, "A轮"));
        moneyFlowRepository.saveAndFlush(createFlow("investor", "self", 20002, "A轮"));
        moneyFlowRepository.saveAndFlush(createFlow("investor", "self", 30002, "B轮"));

        moneyFlowRepository.saveAndFlush(createFlow("channel", "self", 10000, "天猫"));
        moneyFlowRepository.saveAndFlush(createFlow("channel", "self", 20000, "天猫"));
        moneyFlowRepository.saveAndFlush(createFlow("channel", "self", 30000, "天猫"));
        moneyFlowRepository.saveAndFlush(createFlow("channel", "self", 40000, "京东"));

        moneyFlowRepository.saveAndFlush(createFlow("self", "channel", -5000, "京东"));
        moneyFlowRepository.saveAndFlush(createFlow("self", "channel", -4000, "京东"));
        moneyFlowRepository.saveAndFlush(createFlow("self", "channel", -3000, "天猫"));
        moneyFlowRepository.saveAndFlush(createFlow("self", "channel", -2000, "天猫"));

        restMoneyFlowMockMvc.perform(get("/api/moneyFlows/byParticipant/investor/grouped")
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.[*].source").value(hasItem("investor")))
            .andExpect(jsonPath("$.[*].dest").value(hasItem("self")))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(30004.0)))
            .andExpect(jsonPath("$.[*].subGroup").value(hasItem("A轮")));

        restMoneyFlowMockMvc.perform(get("/api/moneyFlows/byParticipant/channel/grouped")
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.[*].source").value(hasItem("channel")))
            .andExpect(jsonPath("$.[*].source").value(hasItem("self")))
            .andExpect(jsonPath("$.[*].dest").value(hasItem("self")))
            .andExpect(jsonPath("$.[*].dest").value(hasItem("channel")))
            .andExpect(jsonPath("$.[*].subGroup").value(hasItem("天猫")))
            .andExpect(jsonPath("$.[*].subGroup").value(hasItem("京东")))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(60000.0)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(40000.0)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(-9000.0)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(-5000.0)));
    }

    private MoneyFlow createFlow(String source, String dest, long amount, String subGroup) {
        MoneyFlow flow = new MoneyFlow();
        flow.setSource(source);
        flow.setDest(dest);
        flow.setAmount(BigDecimal.valueOf(amount));
        flow.setDate(DEFAULT_DATE);
        flow.setSubGroup(subGroup);
        return flow;
    }
}
