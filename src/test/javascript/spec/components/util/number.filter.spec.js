'use strict';

describe('Filter Test', function () {
    var $filter;

    beforeEach(module('bamaiApp'));

    beforeEach(inject(function(_$filter_) {
        $filter = _$filter_;
    }));

    describe('numberIn10K', function () {
        var numberIn10K;

        it('should convert number greater than 10000 to string keep several digit after point', 
        function () {
            numberIn10K = $filter('numberIn10K');

            expect(numberIn10K(2000200)).toBe('200.02万');
            expect(numberIn10K(2000200, 0)).toBe('200万');
            expect(numberIn10K(2000200, -1)).toBe('200.02万');
            expect(numberIn10K(2000200, -10)).toBe('200.02万');

            expect(numberIn10K(10000)).toBe('1.00万');
            expect(numberIn10K(10000, 0)).toBe('1万');
            expect(numberIn10K(10000, 5)).toBe('1.00000万');
            expect(numberIn10K(10000, -1)).toBe('1.00万');

            expect(numberIn10K(100)).toBe('0.01万');
            expect(numberIn10K(100, 0)).toBe('0万');
            expect(numberIn10K(100, -1)).toBe('0.01万');

            expect(numberIn10K(0)).toBe('0.00万');
            expect(numberIn10K(0, 0)).toBe('0万');
            expect(numberIn10K(0, 3)).toBe('0.000万');
            expect(numberIn10K(0, -1)).toBe('0.00万');
            expect(numberIn10K(0, -20)).toBe('0.00万');

            expect(numberIn10K(-121)).toBe('-0.01万');
            expect(numberIn10K(-200)).toBe('-0.02万');
            expect(numberIn10K(-2000, 0)).toBe('-0万');
            expect(numberIn10K(-2000, 3)).toBe('-0.200万');
            expect(numberIn10K(-2000, -1)).toBe('-0.20万');
            expect(numberIn10K(-2000, -100)).toBe('-0.20万');
        });
    });
});
