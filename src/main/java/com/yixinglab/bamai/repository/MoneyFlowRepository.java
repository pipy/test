package com.yixinglab.bamai.repository;

import com.yixinglab.bamai.web.rest.dto.GroupedFlowDTO;
import com.yixinglab.bamai.domain.MoneyFlow;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MoneyFlow entity.
 */
public interface MoneyFlowRepository extends JpaRepository<MoneyFlow,Long> {

    @Query("select new com.yixinglab.bamai.web.rest.dto.GroupedFlowDTO(f.source, f.dest, sum(f.amount), f.subGroup) " +
           "from MoneyFlow f where f.source=?1 or f.dest=?1 " +
           "group by f.source, f.dest, f.subGroup")
    List<GroupedFlowDTO> findGroupedFlowSums(String flowName);
}
