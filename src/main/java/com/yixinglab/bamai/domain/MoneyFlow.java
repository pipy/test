package com.yixinglab.bamai.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A MoneyFlow.
 */
@Entity
@Table(name = "money_flow")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MoneyFlow implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "source", nullable = false)
    private String source;
    
    @NotNull
    @Column(name = "dest", nullable = false)
    private String dest;
    
    @NotNull
    @Column(name = "amount", precision=10, scale=2, nullable = false)
    private BigDecimal amount;
    
    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;
    
    @NotNull
    @Column(name = "sub_group", nullable = false)
    private String subGroup;
    
    @Column(name = "comment")
    private String comment;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }
    
    public void setSource(String source) {
        this.source = source;
    }

    public String getDest() {
        return dest;
    }
    
    public void setDest(String dest) {
        this.dest = dest;
    }

    public BigDecimal getAmount() {
        return amount;
    }
    
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }
    
    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSubGroup() {
        return subGroup;
    }
    
    public void setSubGroup(String subGroup) {
        this.subGroup = subGroup;
    }

    public String getComment() {
        return comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MoneyFlow moneyFlow = (MoneyFlow) o;
        if(moneyFlow.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, moneyFlow.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MoneyFlow{" +
            "id=" + id +
            ", source='" + source + "'" +
            ", dest='" + dest + "'" +
            ", amount='" + amount + "'" +
            ", date='" + date + "'" +
            ", subGroup='" + subGroup + "'" +
            ", comment='" + comment + "'" +
            '}';
    }
}
