/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.yixinglab.bamai.web.rest.dto;
