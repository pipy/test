package com.yixinglab.bamai.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.yixinglab.bamai.domain.MoneyFlow;
import com.yixinglab.bamai.repository.MoneyFlowRepository;
import com.yixinglab.bamai.web.rest.dto.GroupedFlowDTO;
import com.yixinglab.bamai.web.rest.util.HeaderUtil;
import com.yixinglab.bamai.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MoneyFlow.
 */
@RestController
@RequestMapping("/api")
public class MoneyFlowResource {

    private final Logger log = LoggerFactory.getLogger(MoneyFlowResource.class);

    @Inject
    private MoneyFlowRepository moneyFlowRepository;

    /**
     * POST  /moneyFlows -> Create a new moneyFlow.
     */
    @RequestMapping(value = "/moneyFlows",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MoneyFlow> createMoneyFlow(@Valid @RequestBody MoneyFlow moneyFlow) throws URISyntaxException {
        log.debug("REST request to save MoneyFlow : {}", moneyFlow);
        if (moneyFlow.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("moneyFlow", "idexists", "A new moneyFlow cannot already have an ID")).body(null);
        }
        MoneyFlow result = moneyFlowRepository.save(moneyFlow);
        return ResponseEntity.created(new URI("/api/moneyFlows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("moneyFlow", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /moneyFlows -> Updates an existing moneyFlow.
     */
    @RequestMapping(value = "/moneyFlows",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MoneyFlow> updateMoneyFlow(@Valid @RequestBody MoneyFlow moneyFlow) throws URISyntaxException {
        log.debug("REST request to update MoneyFlow : {}", moneyFlow);
        if (moneyFlow.getId() == null) {
            return createMoneyFlow(moneyFlow);
        }
        MoneyFlow result = moneyFlowRepository.save(moneyFlow);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("moneyFlow", moneyFlow.getId().toString()))
            .body(result);
    }

    /**
     * GET  /moneyFlows/byParticipant/{id}/grouped -> get all grouped transactions of specified participant
     */
    @RequestMapping(value = "/moneyFlows/byParticipant/{id}/grouped",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GroupedFlowDTO>> getGroupedFlowsByParticipant(@PathVariable(value="id") String flowId)
        throws URISyntaxException {
        log.debug("REST request to getGroupedFlowsByParticipant");
        List<GroupedFlowDTO> grouped = moneyFlowRepository.findGroupedFlowSums(flowId);
        return new ResponseEntity<>(grouped, HttpStatus.OK);
    }

    /**
     * GET  /moneyFlows -> get all the moneyFlows.
     */
    @RequestMapping(value = "/moneyFlows",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<MoneyFlow>> getAllMoneyFlows(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MoneyFlows");
        Page<MoneyFlow> page = moneyFlowRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/moneyFlows");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /moneyFlows/:id -> get the "id" moneyFlow.
     */
    @RequestMapping(value = "/moneyFlows/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MoneyFlow> getMoneyFlow(@PathVariable Long id) {
        log.debug("REST request to get MoneyFlow : {}", id);
        MoneyFlow moneyFlow = moneyFlowRepository.findOne(id);
        return Optional.ofNullable(moneyFlow)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /moneyFlows/:id -> delete the "id" moneyFlow.
     */
    @RequestMapping(value = "/moneyFlows/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteMoneyFlow(@PathVariable Long id) {
        log.debug("REST request to delete MoneyFlow : {}", id);
        moneyFlowRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("moneyFlow", id.toString())).build();
    }
}
