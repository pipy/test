package com.yixinglab.bamai.web.rest.dto;

import java.math.BigDecimal;

/**
 * DTO for grouped flow data.
 */
public class GroupedFlowDTO {

    private String source;

    private String dest;

    private BigDecimal amount;

    private String subGroup;

    public GroupedFlowDTO(String source, String dest, BigDecimal amount, String subGroup) {
        this.source = source;
        this.dest = dest;
        this.amount = amount;
        this.subGroup = subGroup;
    }

    public String getSource() {
        return source;
    }

    public String getDest() {
        return dest;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getSubGroup() {
        return subGroup;
    }

}
