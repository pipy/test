'use strict';

angular.module('bamaiApp')
    .filter('numberIn10K', function() {
        return function(input, fractionSize) {
            fractionSize =  fractionSize === undefined || fractionSize < 0 ? 2 : fractionSize;

            return (input / 10000).toFixed(fractionSize) + '万';
        };
    });
