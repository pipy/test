'use strict';

angular.module('bamaiApp')
    .factory('MoneyFlow', function ($resource, DateUtils) {
        return $resource('api/moneyFlows/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertLocaleDateFromServer(data.date);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.date = DateUtils.convertLocaleDateToServer(data.date);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.date = DateUtils.convertLocaleDateToServer(data.date);
                    return angular.toJson(data);
                }
            },
            'queryGroupedFlows': { 
                method: 'GET', 
                isArray: true, 
                url: 'api/moneyFlows/byParticipant/:id/grouped'
            }
        });
    });
