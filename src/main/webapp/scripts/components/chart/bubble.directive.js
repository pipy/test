// Copyright (c) 2016 Yixing Lab. All rights reserved.

'use strict';

/**
 * Bubble chart with links
 *
 * Based on http://bl.ocks.org/d3noob/5155181.
 * See http://zhuanlan.zhihu.com/p/20706807 for docs.
 */
angular.module('bamaiApp')
    .directive('linkedBubble', function (lodash) {
        return {
            restrict: 'E',
            scope: {
                // one way scope '<' doesn't exist in Angular 1.4.x
                'data': '=',
                'options': '=?'
            },
            link: function(scope, element, attrs) {
                scope.api = {
                    // Fully refresh directive
                    // TODO: partial refresh with transitions
                    refresh: function() {
                        d3.select(element[0]).select('svg').remove();

                        var svg = d3.select(element[0]).append('svg');

                        // FIXME: Move these to options
                        var height = 400;
                        var width = element[0].parentElement.clientWidth;

                        svg.attr('width', '100%').style({width: '100%'})
                            .attr('height', height).style({'height': height});

                        var links = scope.data.edges;
                        var nodes = scope.data.nodes;

                        if (!links) {
                            // data not prepared
                            return;
                        }

                        var colVals = d3.scale.category10();

                        var centerNode = lodash.find(nodes, ['centered', true]);
                        if (centerNode) {
                            centerNode.fixed = true;
                            centerNode.x = width / 2;
                            centerNode.y = height / 2;
                        }

                        var linkDistance = 160;
                        var force = d3.layout.force()
                            .nodes(nodes)
                            .links(links)
                            .size([width, height])
                            .linkDistance(linkDistance)
                            .charge(-100)
                            .chargeDistance(width)
                            .on('tick', tick)
                            .start();

                        // Set the range
                        var  v = d3.scale.linear().range([0, 100]);
                        // Scale the range of the data
                        v.domain([0, d3.max(links, d => d.value)]);

                        // asign a type per value to encode opacity
                        links.forEach(link => {
                            var scaled = v(link.value);
                            if (scaled <= 25) {
                                link.type = 'twofive';
                            } else if (scaled <= 50) {
                                link.type = 'fivezero';
                            } else if (scaled <= 75) {
                                link.type = 'sevenfive';
                            } else {
                                link.type = 'onezerozero';
                            }
                        });

                        // set up radius range scale
                        var rscale = d3.scale.sqrt()
                            .domain([lodash.minBy(nodes, '_weight')._weight,
                                     lodash.maxBy(nodes, '_weight')._weight])
                            .range([25, 45]);

                        // build the arrows
                        var defs = svg.append('svg:defs');
                        defs.append('svg:marker')
                                .attr('id', 'end')
                                .attr('viewBox', '0 -5 10 10')
                                .attr('refX', 10)
                                .attr('refY', 0)
                                .attr('markerWidth', 6)
                                .attr('markerHeight', 6)
                                .attr('orient', 'auto')
                            .append('svg:path')
                                .attr('d', 'M0,-5L10,0L0,5');

                        defs.append('svg:marker')
                                .attr('id', 'start')
                                .attr('viewBox', '0 -5 10 10')
                                .attr('refX', 0)
                                .attr('refY', 0)
                                .attr('markerWidth', 6)
                                .attr('markerHeight', 6)
                                .attr('orient', 'auto')
                            .append('svg:path')
                                .attr('d', 'M0,0L10,-5L10,5');

                        // add the links and the arrows
                        var path = svg.append('svg:g').selectAll('path')
                            .data(force.links())
                            .enter().append('svg:path')
                            .attr('class', d => 'link ' + d.type)
                            .attr('id', (d,i) => 'edgepath'+i);

                        var edgelabels = svg.selectAll('.edgelabel')
                            .data(force.links())
                            .enter()
                            .append('text')
                            .style('pointer-events', 'none')
                            .attr({'class': d => 'edgelabel ' + d.type,
                                'id': (d,i) => 'edgelabel' + i,
                                'dx': d => (linkDistance - rscale(d.source._weight) - rscale(d.target._weight) - 5) / 2,
                                // FIXME: these belong to CSS
                                'text-anchor': 'middle',
                                'font-weight': 'bold',
                                'font-size': 11,
                                'fill': '#333'});

                        edgelabels.append('textPath')
                            .attr('xlink:href', (d,i) => '#edgepath' + i)
                            .style('pointer-events', 'none')
                            .text(d => d.name);

                        // define the nodes
                        var node = svg.selectAll('.node')
                            .data(force.nodes())
                            .enter()
                            .append('a').attr('xlink:href', d => d.href)
                            .append('g').attr('class', 'node')
                            //.on('click', click)
                            //.on('dblclick', dblclick)
                            .call(force.drag);

                        // add the nodes
                        node.append('circle')
                            .attr('r', d => rscale(d._weight))
                            .style('fill', (d,i) => colVals(i))
                            .style('opacity', 0.5);

                        // add the text
                        node.append('text')
                            .attr('x', 0)
                            // FIXME: these belong to CSS
                            .attr('dominant-baseline', 'middle')
                            .attr('alignment-baseline', 'middle')
                            .attr('font-size', 16)
                            .attr('text-anchor', 'middle')
                            .text(d => d.name);

                        // add subtitle (if present)
                        node.append('text')
                            .attr('x', 0)
                            .attr('y', 12)
                            // FIXME: these belong to CSS
                            .attr('font-size', 12)
                            .attr('dominant-baseline', 'text-before-edge')
                            .attr('text-anchor', 'middle')
                            .text(d => d.subtitle || '');

                        // add the curvy lines
                        function tick() {
                            path.attr('d', d => {
                                var dx = d.target.x - d.source.x,
                                    dy = d.target.y - d.source.y,
                                    dr = Math.sqrt(dx * dx + dy * dy);
                                var k1 = dr / dx,
                                    k2 = dr / dy,
                                    srcR = rscale(d.source._weight),
                                    targetR = rscale(d.target._weight),
                                    x1 = d.source.x + srcR / k1,
                                    y1 = d.source.y + srcR / k2,
                                    x2 = d.target.x - targetR / k1,
                                    y2 = d.target.y - targetR / k2,
                                    r = dr - srcR - targetR;
                                if (x1 < x2) {
                                    return 'M' +
                                        x1 + ',' + y1 + 'A' +
                                        r + ',' + r + ' 0 0,1 ' +
                                        x2 + ',' + y2;
                                } else {
                                    return 'M' +
                                        x2 + ',' + y2 + 'A' +
                                        r + ',' + r + ' 0 0,0 ' +
                                        x1 + ',' + y1;
                                }
                            })
                            .attr('marker-end', d => d.source.x < d.target.x ? 'url(#end)' : '')
                            .attr('marker-start', d => d.source.x >= d.target.x ? 'url(#start)' : '');

                            edgelabels.attr('dominant-baseline', d => d.source.x < d.target.x ?
                                'text-after-edge' : 'text-before-edge');

                            node.attr('transform', d => 'translate(' + d.x + ',' + d.y + ')');
                        }
                    }
                };

                // Watching on data changing
                scope.$watch('data', (newData, oldData) => {
                    if (newData !== oldData) {
                        scope.api.refresh();
                    }
                });

                // Watching on config changing
                scope.$watch('config', (newConfig, oldConfig) => {
                    if (newConfig !== oldConfig) {
                        //scope._config = angular.extend(defaultConfig, newConfig);
                        //scope.api.refresh();
                    }
                }, true);

            }
        };
    });
