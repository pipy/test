'use strict';

angular.module('bamaiApp')
    .controller('MoneyFlowDetailController', function ($scope, $rootScope, $stateParams, entity, MoneyFlow) {
        $scope.moneyFlow = entity;
        $scope.load = function (id) {
            MoneyFlow.get({id: id}, function(result) {
                $scope.moneyFlow = result;
            });
        };
        var unsubscribe = $rootScope.$on('bamaiApp:moneyFlowUpdate', function(event, result) {
            $scope.moneyFlow = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
