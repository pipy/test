'use strict';

angular.module('bamaiApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('moneyFlow', {
                parent: 'entity',
                url: '/moneyFlows',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'bamaiApp.moneyFlow.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/moneyFlow/moneyFlows.html',
                        controller: 'MoneyFlowController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('moneyFlow');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('moneyFlow.detail', {
                parent: 'entity',
                url: '/moneyFlow/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'bamaiApp.moneyFlow.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/moneyFlow/moneyFlow-detail.html',
                        controller: 'MoneyFlowDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('moneyFlow');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MoneyFlow', function($stateParams, MoneyFlow) {
                        return MoneyFlow.get({id : $stateParams.id});
                    }]
                }
            })
            .state('moneyFlow.new', {
                parent: 'moneyFlow',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/moneyFlow/moneyFlow-dialog.html',
                        controller: 'MoneyFlowDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    source: null,
                                    dest: null,
                                    amount: null,
                                    date: null,
                                    subGroup: null,
                                    comment: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('moneyFlow', null, { reload: true });
                    }, function() {
                        $state.go('moneyFlow');
                    })
                }]
            })
            .state('moneyFlow.edit', {
                parent: 'moneyFlow',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/moneyFlow/moneyFlow-dialog.html',
                        controller: 'MoneyFlowDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MoneyFlow', function(MoneyFlow) {
                                return MoneyFlow.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('moneyFlow', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('moneyFlow.delete', {
                parent: 'moneyFlow',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/moneyFlow/moneyFlow-delete-dialog.html',
                        controller: 'MoneyFlowDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['MoneyFlow', function(MoneyFlow) {
                                return MoneyFlow.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('moneyFlow', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
