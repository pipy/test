'use strict';

angular.module('bamaiApp')
    .controller('MoneyFlowController', function ($scope, $state, MoneyFlow, ParseLinks) {

        $scope.moneyFlows = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            MoneyFlow.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.moneyFlows = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.moneyFlow = {
                source: null,
                dest: null,
                amount: null,
                date: null,
                subGroup: null,
                comment: null,
                id: null
            };
        };
    });
