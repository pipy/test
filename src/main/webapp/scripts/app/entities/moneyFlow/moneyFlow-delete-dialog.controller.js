'use strict';

angular.module('bamaiApp')
	.controller('MoneyFlowDeleteController', function($scope, $uibModalInstance, entity, MoneyFlow) {

        $scope.moneyFlow = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            MoneyFlow.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
