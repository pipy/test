'use strict';

angular.module('bamaiApp').controller('MoneyFlowDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'MoneyFlow',
        function($scope, $stateParams, $uibModalInstance, entity, MoneyFlow) {

        $scope.moneyFlow = entity;
        $scope.load = function(id) {
            MoneyFlow.get({id : id}, function(result) {
                $scope.moneyFlow = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('bamaiApp:moneyFlowUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.moneyFlow.id != null) {
                MoneyFlow.update($scope.moneyFlow, onSaveSuccess, onSaveError);
            } else {
                MoneyFlow.save($scope.moneyFlow, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForDate = {};

        $scope.datePickerForDate.status = {
            opened: false
        };

        $scope.datePickerForDateOpen = function($event) {
            $scope.datePickerForDate.status.opened = true;
        };
}]);
