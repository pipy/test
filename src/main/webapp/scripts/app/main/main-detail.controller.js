'use strict';

angular.module('bamaiApp')
    .controller('MainDetailController', function($scope, $stateParams, Principal, MoneyFlow,
    lodash) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            if (!$scope.isAuthenticated()) {
                return;
            }

            //FIXME: the participant theme map should come from a config or service.
            var participantThemeMap = {
                'channel': {
                    style: 'orangeGreenStyle'
                },
                'investor': {
                    title: '累计投资'
                }
            };

            $scope.participant = $stateParams.id;

            $scope.getStyleOfParticipant = function(name) {
                name = participantThemeMap[name];
                if (name) { name = name.style; }

                return name ? name : 'participant-default-style';
            };

            $scope.getTitleOfParticipant = function(name) {
                name = participantThemeMap[name];
                if (name) { name = name.title; }

                return name ? name : '支出';
            };

            $scope.moneyFlowDetail = [];
            $scope.summary = { totalIncome: 0, totalExpenditure: 0 };

            function sortByGroup(array) {
                return lodash.chain(array).groupBy('subGroup').mapValues(function(flows) {
                    return {
                        'flows': lodash.orderBy(flows, 'amount'),
                        'total': lodash.sumBy(flows, function(flow) {
                            return Math.abs(flow.amount);
                        })
                    };
                }).values().orderBy('total', 'desc').flatMap(function(o) {
                    return o.flows;
                }).value();
            }

            function sortByVcRound(array) {
                var vcRounds = ['天使轮', 'A轮', 'B轮'];

                return lodash.chain(array).groupBy('subGroup').mapValues(function(flows) {
                    return {
                        'flows': lodash.orderBy(flows, 'amount'),
                        'index': lodash.map(flows, function(flow) {
                            return vcRounds.indexOf(flow.subGroup);
                        })
                    };
                }).values().orderBy('index').flatMap(function(o) {
                    return o.flows;
                }).value();
            }

            function loadData() {
                MoneyFlow.queryGroupedFlows({id: $scope.participant}, function(result) {
                    var sortFn = $stateParams.id === 'investor' ? sortByVcRound : sortByGroup;
                    var dataParts = lodash.partition(result, function(data) {
                        return data.amount >= 0;
                    });

                    $scope.moneyFlowDetail = sortFn(result);
                    $scope.summary.totalIncome = lodash.sumBy(dataParts[0], 'amount');
                    $scope.summary.totalExpenditure = lodash.sumBy(dataParts[1], 'amount');
                });
            }

            loadData();
        });
    });
