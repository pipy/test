'use strict';

angular.module('bamaiApp')
    .controller('MainController', function($scope, Principal, MoneyFlow, lodash, DateUtils,
            $filter, $state) {
        Principal.identity().then(account => {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            if (!$scope.isAuthenticated()) {
                return;
            }

            var defaultLineChartOptions = {
                chart: {
                    type: 'lineChart',
                    margin: {
                        top: 20,
                        right: 30,
                        bottom: 40,
                        left: 70
                    },
                    x: d => d.date,
                    y: d => d.amount,
                    showControls: false,
                    useInteractiveGuideline: true,
                    xAxis: { tickFormat: d => $filter('date')(d, 'yyyy-MM-dd') },
                    yAxis: { tickFormat: d => $filter('number')(d, 0) }
                }
            };

            var tabNames = ['累计投资', '账面现金', '累计收入'];

            $scope.slider = {};

            $scope.displayStartDate = null;
            $scope.displayEndDate = null;
            $scope.originalData = [];

            function updateFlowChart(allFlows) {
                // FIXME: move to config
                var displayNameMapping = {
                    'employee': '员工',
                    'customer': '用户',
                    'channel': '渠道',
                    'investor': '投资',
                    'tax': '税款',
                    'other': '其他',
                    'self': '资金'
                };
                var flowsWithSum = Object.keys(allFlows).map(flowName => {
                    var [src, dest] = flowName.split('-');
                    var sum = lodash.sumBy(allFlows[flowName], 'amount');
                    return {'source': src, 'target': dest, 'sum': sum};
                });

                var nodesMap = lodash.chain(flowsWithSum)
                    .flatMap(flow => [
                        {'name': flow.source, 'flowSum': flow.sum},
                        {'name': flow.target, 'flowSum': flow.sum}
                    ])
                    .groupBy('name')
                    .mapValues(subNodes => ({
                        'name': displayNameMapping[subNodes[0].name],
                        // 'weight' is used internally by D3 Force layout
                        '_weight': subNodes.reduce( (max, current) =>
                            Math.max(max, Math.abs(current.flowSum)), 0),
                        'href': $state.href('home.detail', {id: subNodes[0].name})
                    })).value();

                if ('self' in nodesMap) {
                    var totalCash = lodash.sumBy(flowsWithSum, 'sum');
                    nodesMap.self.subtitle = $filter('numberIn10K')(totalCash, 0);
                    nodesMap.self.centered = true;
                }

                var edges = flowsWithSum.map(flow => ({
                    'source': nodesMap[flow.source],
                    'target': nodesMap[flow.target],
                    'value': Math.abs(flow.sum),
                    'name': $filter('numberIn10K')(Math.abs(flow.sum), 0)
                }));

                $scope.bubbleData = {
                    'nodes': lodash.values(nodesMap),
                    'edges': edges
                };
            }

            /**
             * Reducer function for accumulating over a trascation list sorted by date.
             * Elements with same dates will be merged.
             *
             * prev: object {'total': number, 'list': array}
             * elem: object {'date': Date, 'amount': number}
             */
            function accumulateByDate(prev, elem) {
                prev.total += elem.amount;
                var list = prev.list;
                if (list.length > 0 && list[list.length - 1].date.getTime() === elem.date.getTime()) {
                    list[list.length - 1].amount = prev.total;
                } else {
                    prev.list = prev.list.concat({'date': elem.date, 'amount': prev.total});
                }
                return prev;
            }

            function updateTabs(allFlows) {
                var investments = [];
                if ('investor-self' in allFlows) {
                    investments = allFlows['investor-self'];
                }
                var accumInvestment = investments.reduce(accumulateByDate, {'total': 0, list: []}).list;

                var accumCash = lodash.chain(allFlows).values().flatten()
                    .sort((left, right) => left.date.getTime() - right.date.getTime())
                    .reduce(accumulateByDate, {'total': 0, list: []}).value().list;

                var accumRevenue = lodash.chain(lodash.values(lodash.pick(allFlows, ['channel-self', 'customer-self'])))
                    .flatten().sort((left, right) => left.date.getTime() - right.date.getTime())
                    .reduce(accumulateByDate, {'total': 0, list: []}).value().list;

                var chartsData = [accumInvestment, accumCash, accumRevenue];
                var colors = d3.scale.category10();
                chartsData.forEach( (d, i) => {
                    $scope.tabs[i].chart.data = [{
                        'area': true,
                        'strokeWidth': 2,
                        'key': tabNames[i],
                        'values': d,
                        'color': colors(i)
                    }];
                });
            }

            function updateCharts(startDate, endDate) {
                if ($scope.displayStartDate === null || $scope.displayEndDate === null ||
                        startDate.getTime() !== $scope.displayStartDate.getTime() ||
                        endDate.getTime() !== $scope.displayEndDate.getTime()) {
                    var dataInRange = $scope.originalData.filter(d => {
                        var millis = d.date.getTime();
                        return millis >= startDate.getTime() && millis <= endDate.getTime();
                    });
                    var allFlows = lodash.groupBy(dataInRange, d => d.source + '-' + d.dest);

                    // TODO: no need to recalculate everything on date range slider change.
                    // Should keep the first results and just filter by date range.
                    // To recreate datapoint objects means no nice d3 transition animations.
                    updateFlowChart(allFlows);
                    updateTabs(allFlows);

                    $scope.displayStartDate = startDate;
                    $scope.displayEndDate = endDate;
                }
            }

            function createSlider(origStartDate, origEndDate) {
                var firstMonth = DateUtils.dateToMonths(origStartDate);
                var lastMonth = DateUtils.dateToMonths(origEndDate);

                $scope.slider = {
                    minValue: 0,
                    maxValue: lastMonth - firstMonth,
                    options: {
                        floor: 0,
                        ceil: lastMonth - firstMonth,
                        minRange: 1,
                        noSwitching: true,

                        translate: value => DateUtils.monthsToString(firstMonth + value),

                        onEnd: (sliderId, modelValue, highValue) => {
                            var startDate = DateUtils.monthsToDate(firstMonth + modelValue);
                            var endDate = new Date(DateUtils.monthsToDate(firstMonth + highValue + 1).getTime() - 1);
                            updateCharts(startDate, endDate);
                        }
                    }
                };
            }

            $scope.tabs = tabNames.map(name => ({
                'head': name,
                'chart': {
                    'options': defaultLineChartOptions,
                    'data': [
                        {
                            'key': name,
                            'values': [],
                            'color': '#ff7f0e'
                        }
                    ]
                }
            }));

            $scope.loadData = function(startPage) {
                MoneyFlow.query(
                    // Spring Data Rest won't sent more than 2000 records per query,
                    // and we don't want to add a new API for this yet.
                    // Hence the chained multiple queries here.
                    { page: startPage, size: 2000, sort: ['date'] },
                    (result, headers) => {
                        var totalItems = headers('X-Total-Count');
                        if (totalItems > 0) {
                            $scope.originalData = $scope.originalData.concat(result);
                            var rs = $scope.originalData;
                            if (rs.length < totalItems) {
                                $scope.loadData(startPage + 1);
                            } else {
                                rs.forEach(d => {
                                    d.date = new Date(d.date);
                                });
                                var startDate = rs[0].date;
                                var endDate = rs[rs.length - 1].date;
                                updateCharts(startDate, endDate);
                                createSlider(startDate, endDate);
                            }
                        } else {
                            // TODO: hide slider when no data retrieved
                        }
                    }
                );
            };

            $scope.loadData(0);

        });
    });
