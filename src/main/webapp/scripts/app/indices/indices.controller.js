'use strict';

angular.module('bamaiApp')
    .controller('indicesController', ['$scope', '$filter', 'Principal', 'lodash', function(
    $scope, $filter, Principal, lodash) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            if (!$scope.isAuthenticated()) {
                return;
            }

            var mockData = [
                {
                    name: '日均完单量',
                    isPrimary: true,
                    valueUnit: '%',
                    chartType: 'area',
                    data: [
                        //TODO: Each data has same start date and end date?
                        {
                            amount: 100000,
                            date: new Date('2015-10-02')
                        },
                        {
                            amount: 200000,
                            date: new Date('2015-11-20')
                        },
                        {
                            amount: 100000,
                            date: new Date('2016-01-20')
                        }
                    ]
                },
                {
                    name: '日均访问量',
                    isPrimary: false,
                    valueUnit: '单',
                    chartType: 'line',
                    data: [
                        //TODO: Each data has same start date and end date?
                        {
                            amount: 100000,
                            date: new Date('2012-10-02')
                        },
                        {
                            amount: 200000,
                            date: new Date('2013-11-20')
                        },
                        {
                            amount: 100000,
                            date: new Date('2014-01-20')
                        }
                    ]
                },
                {
                    name: '高价值用户',
                    isPrimary: false,
                    valueUnit: '单',
                    chartType: 'area',
                    data: [
                        //TODO: Each data has same start date and end date?
                        {
                            amount: 2000,
                            date: new Date('2015-10-02')
                        },
                        {
                            amount: 20000,
                            date: new Date('2015-11-20')
                        },
                        {
                            amount: 100000,
                            date: new Date('2016-01-20')
                        }
                    ]
                }
            ];

            //TODO: Date format.
            $scope.slider = {
                options: {
                    noSwitching: true
                },
                minValue: 0,
                maxValue: 1
            };

            $scope.primary = [];
            $scope.secondary = [];
            $scope.chart = {
                options: {
                    chart: {
                        type: 'lineChart',
                        margin: {
                            top: 20,
                            right: 30,
                            bottom: 40,
                            left: 70
                        },
                        x: d => d.date,
                        y: d => d.amount,
                        showControls: false,
                        useInteractiveGuideline: true,
                        xAxis: { tickFormat: d => $filter('date')(d, 'yyyy-MM-dd') },
                        yAxis: { tickFormat: d => $filter('number')(d, 0) }
                    }
                }
            };

            $scope.switchChart = function(data) {
                if ($scope.currentChart === data.name) {
                    return;
                }

                let colors = d3.scale.category10().range();
                let n = Math.ceil(Math.random() * 10);

                $scope.currentChart = data.name;
                $scope.chart.data = [{
                    area: data.chartType,
                    key: data.name,
                    values: data.data,
                    color: colors[n]
                }];
            };

            $scope.isChartActive = data => $scope.currentChart === data.name;

            function loadData() {
                let data = lodash.groupBy(mockData, 'isPrimary');

                $scope.primary = data.true;
                $scope.secondary = data.false;

                $scope.currentChart = data.true[0].name;
                $scope.chart.data = [{
                    area: data.true[0].chartType,
                    key: data.true[0].name,
                    values: data.true[0].data,
                    color: '#f60'
                }];
            }

            loadData();
        });
    }]);
