'use strict';

angular.module('bamaiApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('indices', {
                parent: 'site',
                url: '/indices',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/indices/indices.html',
                        controller: 'indicesController'
                    }
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                        $translatePartialLoader.addPart('main');
                        return $translate.refresh();
                    }]
                }
            });
    });
