资金流向数据处理示例
====================

存放用于处理和演示资金流向数据提取的Jupyter Notebook示例.

文件清单:

* `mocar_complete_201511.csv`: 截止2015年11月底为止的摩卡爱车财务流水
* `mocar_money_flows.ipynb`: 用于提取各角色资金流向数据的Jupyter Notebook示例
* 其他csv文件: notebook提取的资金流向数据

详情请用Jupyter运行notebook, 查看注释与运行结果.

